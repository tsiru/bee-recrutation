require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  fixtures :users

  let(:user) { users(:user) }

  before { sign_in user }


  describe 'GET #destroy' do
    it 'destroys the requested user' do
      expect {
        get :destroy, { id: user.to_param }
        # there is no such actino in UsersController
        # and it shouldn't be done by get, couse someone can put image into post with path
        # /users/destroy and logged user's account will be destroyed
      }.to change(User, :count).by(-1)
    end
  end
end
