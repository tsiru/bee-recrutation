Rails.application.routes.draw do
  root to: 'posts#index'

  resources :posts

  match 'users/sign_in', via: %i(post get), as: :sign_in
  match 'users/sign_up', via: %i(post get), as: :sign_up
  get   'users/sign_out', as: :sign_out
  # missing destroy action for current user, like:
  # match 'users', via: :delete, to 'users#destroy'
end
