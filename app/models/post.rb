class Post < ActiveRecord::Base
  belongs_to :user

  scope :longer_than, lambda { |length| where("length('body') > #{length}") }

  validates :title, :body, presence: true
  # user_id is not required for purpose?

  # post body before save should be validated, to allow only <i> and <b> tags,
  # bacuse now they allow for any html.
end
