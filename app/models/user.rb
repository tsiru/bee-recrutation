class User < ActiveRecord::Base
  has_many :posts
  validates_uniqueness_of :username
  validates_presence_of :password, :username # and min length
end
