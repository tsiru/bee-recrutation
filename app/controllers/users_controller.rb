class UsersController < ApplicationController
  skip_before_action :verify_authenticity_token

  def sign_in
    if request.post?
      @user = User.find_by(username: user_params[:username])
      if @user.present? && @user.password == user_params[:password]
        # if password was omnited in request, it was possibility
        # to log as enyone without providing password
        session[:user_id] = @user.id
        redirect_to root_path, notice: 'You are now logged in'
      else
        flash.now[:notice] = 'Username or password are invalid'
      end
    else
      @user = User.new
    end
  end

  def sign_up
    if request.post?
      @user = User.new user_params
      if @user.save
        session[:user_id] = @user.id
        redirect_to root_path, notice: 'Your account has been created'
      else
        flash.now[:notice] = @user.errors.full_messages.join(' ')
      end
    else
      @user = User.new
    end
  end

  def sign_out
    session[:user_id] = nil
    redirect_to root_path, notice: 'You have been signed out'
  end

  private

  def user_params
    params.require(:user).permit(:username, :password)
  end
end
