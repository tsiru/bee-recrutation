class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include ApplicationHelper

  def authenticate_user!
    redirect_to sign_in_path, notice: 'You need to be signed in first' unless user_signed_in?
  end
end
