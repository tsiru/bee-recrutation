class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username
      t.string :password
      # password shouln't be save this way, I would rather
      # use devise or at least password hash

      t.timestamps null: false
    end
  end
end
